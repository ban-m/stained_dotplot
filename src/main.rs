extern crate bio;
extern crate clap;
extern crate image;
use clap::{App,Arg};
use std::path::Path;
mod dotplot;
fn main() {
    let matches = App::new("Stained Dotplot")
        .version("0.1")
        .author("Bansho Masutani")
        .about("Creates a dotplot from two fastq files")
        .arg(Arg::with_name("seq1")
             .long("seq1")
             .help("The first fastq file.")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("seq2")
             .long("seq2")
             .help("The second fastq file.")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("outfile")
             .long("outfile")
             .help("The name of output file. Extension is automatically added. e.g. seq1seq2doptlot")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("maxwidth")
             .long("maxwidth")
             .help("Max width of the generated image (default:750")
             .takes_value(true)
             .default_value("750"))
        .arg(Arg::with_name("maxheight")
             .long("maxheight")
             .help("Max height of the generated image(default:750)")
             .takes_value(true)
             .default_value("750"))
        .arg(Arg::with_name("format")
             .long("format")
             .help("Output file format:png,jpg,bmp. (Default:png)")
             .possible_values(&["png","jpg","bmp"])
             .takes_value(true)
             .default_value("png"))
        .arg(Arg::with_name("secondcmp")
             .long("secondcmp")
             .help("use complementaly of second sequence."))
        .arg(Arg::with_name("word")
             .long("word")
             .help("word length for suffix array mode (default:10)")
             .takes_value(true)
             .default_value("10"))
        .arg(Arg::with_name("window")
             .long("window")
             .help("window size for ordinary doplot mode (default: 0)")
             .takes_value(true)
             .default_value("0"))
        .arg(Arg::with_name("from1")
             .long("from1")
             .help("starting position of first sequence. (Default:0)")
             .takes_value(true)
             .default_value("0"))
        .arg(Arg::with_name("to1")
             .long("to1")
             .help("ending position of first sequence. (Default:length of first sequence")
             .takes_value(true)
             .default_value("0"))
        .arg(Arg::with_name("from2")
             .long("from2")
             .help("starting position of second sequence. (Default:0)")
             .takes_value(true)
             .default_value("0"))
        .arg(Arg::with_name("to2")
             .long("to2")
             .help("ending position of second sequence. (Default:length of second sequence")
             .takes_value(true)
             .default_value("0"))
        .get_matches();
    // Main procedure. First, I make sure that input files do exist.
    let seq1 = Path::new(matches.value_of("seq1").unwrap());
    let seq2 = Path::new(matches.value_of("seq2").unwrap());
    if !seq1.exists() || !seq2.exists(){
        println!("Error: sequence {} dosn't exist.",if !seq1.exists(){
            matches.value_of("seq1").unwrap()}
                 else{
                     matches.value_of("seq2").unwrap()
                 });
        return;
    }
    // Create output file.
    let extension = match matches.value_of("format"){
        Some("png") => ".png",
        Some("jpg") => ".jpg",
        Some("bmp") => ".bmp",
        Some(x) => {println!("invalid argument:{}",x);return;},
        None => unreachable!()
    };
    let output = String::from(matches.value_of("outfile").unwrap()) + extension;
    let output = Path::new(&output);
    // Parse other parameters.
    let width:u32 = matches.value_of("maxwidth").unwrap().parse().unwrap();
    let height:u32 = matches.value_of("maxheight").unwrap().parse().unwrap();
    let secondcmp:bool = matches.is_present("secondcmp");
    let word:u32 = matches.value_of("word").unwrap().parse().unwrap();
    let window:u32 = matches.value_of("window").unwrap().parse().unwrap();
    let (from1,to1):(u32,u32) = (matches.value_of("from1").unwrap().parse().unwrap(),
                                 matches.value_of("to1").unwrap().parse().unwrap());
    let (from2,to2):(u32,u32) = (matches.value_of("from2").unwrap().parse().unwrap(),
                                 matches.value_of("to2").unwrap().parse().unwrap());
    // Run
    use dotplot::run;
    if let Err(why) = run(&seq1,&seq2,&output,width,height,secondcmp,word,window,from1,to1,from2,to2){
        eprintln!("Error occured during execution:{}",why)
    }
}
