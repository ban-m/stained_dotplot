use std;
use image;
use std::path::Path;
use bio::{alphabets::dna,io::fastq};
const OFFSET:u8 = 33;
pub fn run(seq1:&Path,seq2:&Path,output:&Path,width:u32,height:u32,secondcmp:bool,word:u32,window:u32,from1:u32,to1:u32,from2:u32,to2:u32)->std::io::Result<()>{
    let seq1 = fastq::Reader::from_file(seq1)?.records().filter_map(|e|e.ok()).nth(0).unwrap();
    let seq2 = fastq::Reader::from_file(seq2)?.records().filter_map(|e|e.ok()).nth(0).unwrap();
    let to1 = if to1==0{ (seq1.seq()).len() as u32 }else{to1};
    let to2 = if to2 == 0 {(seq2.seq()).len() as u32 }else{ to2};
    let seq2 = if secondcmp {revcmp(seq2)}else{seq2};
    let ((seq1,qual1),(seq2,qual2)) = (crop(seq1,from1,to1),crop(seq2,from2,to2));
    dotplot(&seq1,&qual1,&seq2,&qual2,output,width,height,word,window)
}

fn revcmp(rec: fastq::Record)->fastq::Record{
    let id = rec.id();
    let desc = rec.desc();
    let seq = dna::revcomp(rec.seq());
    let qual:Vec<_> = rec.qual().iter().rev().map(|&e|e).collect();
    fastq::Record::with_attrs(id,desc,&seq,&qual)
}

fn crop(rec: fastq::Record,from:u32,to:u32)->(Vec<u8>,Vec<u8>){
    assert!(from <= to);
    let seq:Vec<_> = rec.seq().iter().skip(from as usize).take((to+1-from) as usize)
        .map(|&e|e).collect();
    let qual = rec.qual().iter().skip(from as usize).take((to+1-from) as usize)
        .map(|&e|e).collect();
    (seq,qual)
}


fn dotplot(seq1:&Vec<u8>,qual1:&Vec<u8>,seq2:&Vec<u8>,qual2:&Vec<u8>,output:&Path,width:u32,height:u32,word:u32,window:u32)->std::io::Result<()>{
    let mut buffer: image::ImageBuffer<image::Rgba<u8>,Vec<u8>> = image::ImageBuffer::new(width,height);
    let (len1,len2) = (seq1.len() as u32,seq2.len() as u32);
    let range = word as usize;
    for (i,j,pixel) in buffer.enumerate_pixels_mut(){
        let idx1 = (i * (len1-word) / width) as usize;
        let idx2 = (j * (len2-word) / height) as usize;
        let colour = determine(&seq1[idx1..idx1+range],&qual1[idx1..idx1+range],
                               &seq2[idx2..idx2+range],&qual2[idx2..idx2+range],range);
        *pixel = colour;
    }
    buffer.save(output)
}

fn determine(seq1:&[u8],qual1:&[u8],seq2:&[u8],qual2:&[u8],range:usize)->image::Rgba<u8>{
    image::Rgba([0,
                 0,
                 0,
                 convert((0..range).map(|idx|logprob(seq1[idx],qual1[idx],seq2[idx],qual2[idx])).sum())])
}

fn logprob(s1:u8,q1:u8,s2:u8,q2:u8)->f64{
    let result = if s1 == s2 {
        1. - p(q1) - p(q2) + 4.*p(q1)*p(q2)/3.
    }else{
        ((1.-p(q1)*p(q2)) + p(q1)*(1.-p(q2)))/3.
    };
    result.ln()
}

fn p(x:u8)->f64{
    10f64.powf((x-OFFSET) as f64 / (-10.))
}

fn convert(x:f64)->u8{
    if x > 0.5 {
        255
    }else{
        (x * 255.).floor() as u8
    }
}
