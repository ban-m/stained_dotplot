# Stained Dotplot

A tiny library to draw a dopplot from two fastq files.

## Author
Bansho Masutani: ban-m@g.ecc.u-tokyo.ac.jp

## Usage

### Requirement

- Rust language: > 1.26


### Installation

- Clone this repository
```
git clone []
```
- Run( sudo is not required). It takes a few minutes for the first run because of compilation. Don't forget to pass --release flag.
```
cargo run --release -- --seq1 [] --seq2 [] --outfile [] --maxwidth [] --maxheight[] --format [] --secondcomp --word --window --from1 [] --to1 [] --from2 [] --to2 []
```

To see parameter descriptions in detail, type ```cargo run --release --help ```.

You can also pre-compile the binary file by ```cargo build --release```. Then, you will see the binary at ./target/release/stained_dotplot .
